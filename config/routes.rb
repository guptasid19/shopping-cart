Rails.application.routes.draw do
  require 'sidekiq/web'
  mount Sidekiq::Web => '/sidekiq'
  root "marketplace#index"
 
  devise_for :users, :controllers => {:registrations => "users/registrations",
                                      :omniauth_callbacks => "users/omniauth_callbacks"}
  resources :cart_invoice, only: [:new, :create]
  resource :charges, only: [:new, :create]
  resources :marketplace, only: [:index, :show]
  resources :dashboard, only: [:index]
  resources :cart_items ,only: [:create, :index, :destroy]
  resources :products
  resources :invoices, only: [:new, :create, :index, :show]
  resources :permissions, only: [:index, :update] do
    collection do
      put :update_role
      get :buyers
      get :sellers
    end
  end
  resources :buyers, only: [:index] do
    member do
      get :invoices
    end
  end
  resources :sellers, only: [:index]
  mount ActionCable.server, at: '/cable'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
