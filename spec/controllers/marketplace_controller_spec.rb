require 'rails_helper'

RSpec.describe MarketplaceController, type: :controller do

  let (:user1) {FactoryGirl.create(:user)} 
  let (:user2)  {FactoryGirl.create(:user)}
  let (:product1) {FactoryGirl.create(:product, name: "Pendrive", user_id: user1.id)} 
  let (:product2)  {FactoryGirl.create(:product, name: "Pencil", user_id: user2.id)}
  let (:product3)  {FactoryGirl.create(:product, name: "Dendrite", user_id: user2.id)}

  describe 'get index' do

    it "renders the index page" do
      get :index
      expect(response).to render_template(:index)
    end

    it "selects all the products based on search" do
      get :index
      search ="Pen"
      expect(assigns(:products)).to eq([product1,product2])
    end

  end

  describe 'get show' do

    it "renders the show page" do
      get :show, params: {id: product1.id}
      expect(response).to render_template(:show)
    end

    it "assigns the requested product to @product" do
      get :show, params: {id: product1.id}
      expect(assigns(:product)).to eq(product1)
    end

  end

end
