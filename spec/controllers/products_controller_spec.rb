require 'rails_helper'

RSpec.describe ProductsController, type: :controller do

  let (:user1) {FactoryGirl.create(:user)} 
  let (:user2)  {FactoryGirl.create(:user)}
  let (:product1)  {FactoryGirl.create(:product, user_id: user1.id)}
  let (:product2)  {FactoryGirl.create(:product, user_id: user2.id)}

  describe 'get index' do
    it "selects all products of current user" do
      user1.add_role :seller
      sign_in user1
      get :index
      expect(assigns(:products)).to eq([product1])
    end

    it "renders the index view " do
      user1.add_role :seller
      sign_in user1
      get :index
      expect(response).to render_template(:index)
    end

  end

  describe 'get new' do
    it "assigns a new product to @product" do
      user1.add_role :seller
      sign_in user1
      product3 = FactoryGirl.build(:product, user_id: user1.id)
      get :new
      expect(assigns(:product)).to be_instance_of(Product)
    end

    it "assigns a new product with id of seller" do
      user1.add_role :seller
      sign_in user1
      get :new
      expect(assigns(:product).user_id).to eq(user1.id)
    end

  end

  describe 'post create' do
    context 'with valid attributes' do
      it "saves the product in the database" do
        user1.add_role :seller
        sign_in user1
        expect{ post :create, params: { product: FactoryGirl.attributes_for(:product) } }.to change(Product, :count).by(1)
      end

      it "redirects to products index page" do
        user1.add_role :seller
        sign_in user1
        post :create, params: {product: FactoryGirl.attributes_for(:product)}
        expect(response).to redirect_to(assigns(:index))
      end

    end

  end

  describe 'get show' do
    it 'assigns the requested product to @product' do
      user1.add_role :seller
      sign_in user1
      product = FactoryGirl.create(:product, user_id: user1.id)
      get :show, params: {id: product.id}
      expect(assigns(:product)).to eq(product)
    end

    it 'renders the show template for requested product' do
      user1.add_role :seller
      sign_in user1
      get :show, params: {id: FactoryGirl.create(:product, user_id: user1.id)}
      expect(response).to render_template(:show)
    end

  end

  describe 'delete destroy' do
    
    before(:each) do
      user1.add_role :seller
      sign_in user1
      @product = FactoryGirl.create(:product, user_id: user1.id)
    end

    it "deletes the requested object" do
      expect{ delete :destroy, params: {id: @product}}.to change(Product, :count).by(-1)
    end

    it "redirects to the index page of products" do
      delete :destroy, params: {id: @product}
      expect(response).to redirect_to('/products')
    end

  end

  describe 'get edit' do
    
    before(:each) do
      user1.add_role :seller
      sign_in user1
      @product = FactoryGirl.create(:product, user_id: user1.id)  
    end

    it "assigns the requested product to @product" do
      get :edit, params: { id: @product }
      expect(assigns(:product)).to eq(@product)
    end

    it "renders the edit page for products" do
      get :edit, params: { id: @product }
      expect(response).to render_template(:edit)
    end

  end

  describe 'put update' do

    before(:each) do
      user1.add_role :seller
      sign_in user1
      @product = FactoryGirl.create(:product, user_id: user1.id)
    end

    context "with valid attributes" do
      it "finds the requested @product" do
        put :update, params: { id: @product.id, product: FactoryGirl.attributes_for(:product) }
        expect(assigns(:product)).to eq(@product)
      end

      it "changes @product's attributes" do
        put :update, params: { id: @product.id, product: FactoryGirl.attributes_for(:product, name: "Phone", brand: "Nokia") }
        @product.reload
        expect(@product.name).to  eq("Phone")
        expect(@product.brand).to  eq("Nokia")
      end

      it "redirects to the index page of products" do
        put :update, params: { id: @product.id, product: FactoryGirl.attributes_for(:product) } 
        expect(response).to redirect_to('/products')
      end

    end

  end

end
