require 'rails_helper'

RSpec.describe InvoicesController, type: :controller do

  let (:user1) {FactoryGirl.create(:user)}
  let (:user2) {FactoryGirl.create(:user)}  
  let (:invoice1)  {FactoryGirl.create(:invoice, user_id: user1.id)}
  let (:invoice2)  {FactoryGirl.create(:invoice, user_id: user2.id)}
  let (:invoice3)  {FactoryGirl.create(:invoice, user_id: user1.id)}
  let (:product1)  {FactoryGirl.create(:product, user_id: user2.id)}
  let (:product2)  {FactoryGirl.create(:product, user_id: user2.id)}
  let (:cart_item1)  {FactoryGirl.create(:cart_item, cart_id: user1.cart.id, product_id: product1.id)}
  let (:cart_item2)  {FactoryGirl.create(:cart_item, cart_id: user1.cart.id, product_id: product2.id)}


  describe 'get index' do

    before(:each) do
      sign_in user1
    end

    it "assigns all invoices of buyer to @invoices" do
      get :index
      expect(assigns(:invoices)).to include(invoice1, invoice3)
      expect(assigns(:invoices)).not_to include(invoice2)
    end
    
    it "renders the index page for invoices" do
      get :index
      expect(response).to render_template(:index)
    end

  end  

  describe 'get new' do

    before(:each) do
      sign_in user1
    end

    it "assigns all cart items of a buyer to @cart_items" do
      get :new
      expect(assigns(:cart_items)).to eq([cart_item1, cart_item2])
    end

    it "renders the page for creating a new invoice" do
      get :new
      expect(response).to render_template(:new)
    end

  end

  describe 'post create' do

    before(:each) do
      sign_in user1
    end

    context 'with valid attributes' do
      it "saves the invoice in the database" do
        expect{ post :create, params: { invoice: FactoryGirl.attributes_for(:invoice) } }.to change(Invoice, :count).by(1)
      end

      it "redirects to products index page" do
        post :create, params: {invoice: FactoryGirl.attributes_for(:invoice)}
        expect(response).to redirect_to(assigns(:index))
      end
    end

  end

  describe 'get show' do
    
    before(:each) do
      sign_in user1
      @invoice = FactoryGirl.create(:invoice, user_id: user1.id)
    end
    it 'assigns the requested invoice to @invoice' do
      get :show, params: {id: @invoice.id}
      expect(assigns(:invoice)).to eq(@invoice)
    end

    it 'renders the show template for requested product' do
      get :show, params: {id: @invoice.id}
      expect(response).to render_template(:show)
    end

  end

end

