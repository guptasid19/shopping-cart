require 'rails_helper'

RSpec.describe DashboardController, type: :controller do

  let (:user1) {FactoryGirl.create(:user)} 
  # let (:user2)  {FactoryGirl.create(:user)}
  # let (:user3)  {FactoryGirl.create(:user)}

  describe 'get index' do

    before(:each) do
      user3.add_role :seller
      sign_in user3
      @invoice1 = FactoryGirl.create(:invoice, user_id: user1.id)
      @invoice2 = FactoryGirl.create(:invoice, user_id: user2.id)
      @product1 = FactoryGirl.create(:product, user_id: user3.id)
      @product2 = FactoryGirl.create(:product, user_id: user3.id)
      @product3 = FactoryGirl.create(:product, user_id: user2.id)
      @cart_item1 = FactoryGirl.create(:cart_item, cart_id: user1.cart.id, product_id: @product1.id)
      @cart_item2 = FactoryGirl.create(:cart_item, cart_id: user1.cart.id, product_id: @product2.id)
      @cart_item3 = FactoryGirl.create(:cart_item, cart_id: user2.cart.id, product_id: @product1.id)
      @cart_item4 = FactoryGirl.create(:cart_item, cart_id: user2.cart.id, product_id: @product3.id)
      @invoice_detail1 = FactoryGirl.create(:invoice_detail, invoice_id: @invoice1.id, product_id: @product1.id)
      @invoice_detail2 = FactoryGirl.create(:invoice_detail, invoice_id: @invoice1.id, product_id: @product2.id)
      @invoice_detail3 = FactoryGirl.create(:invoice_detail, invoice_id: @invoice2.id, product_id: @product1.id)
      @invoice_detail4 = FactoryGirl.create(:invoice_detail, invoice_id: @invoice2.id, product_id: @product3.id)
    end

    context " current user is seller" do
      it "assigns all the cart items to @cartitems which have products of that seller added to cart by buyers" do
        get :index
        expect(assigns(:cartitems)).to include(@cart_item1, @cart_item2, @cart_item3)
        expect(assigns(:cartitems)).not_to include(@cart_item4)
      end

      it "assigns all the invoice items to @invoice_items which have products of that seller bought by buyers" do
        get :index
        expect(assigns(:invoice_items)).to include(@invoice_detail2, @invoice_detail1, @invoice_detail3)
        expect(assigns(:invoice_items)).not_to include(@invoice_detail4)
      end

      it "renders the index view page for dashboard" do
        get :index
        expect(response).to render_template(:index)
      end

    end

  end

end
