require 'rails_helper'

RSpec.describe CartItemsController, type: :controller do

  let (:user1) {FactoryGirl.create(:user)} 
  let (:user2)  {FactoryGirl.create(:user)}
  let (:product1)  {FactoryGirl.create(:product, user_id: user1.id)}
  let (:product2)  {FactoryGirl.create(:product, user_id: user2.id)}  
  let (:cart_item2)  {FactoryGirl.create(:cart_item, quantity: 10, cart_id: user1.cart.id, product_id: product2.id)}
  
  describe 'get index' do
    
    before(:each) do
      sign_in user1
    end

    it "selects all cart items of current user" do
      cart_item1 = FactoryGirl.create(:cart_item, quantity: 10, cart_id: user1.cart.id, product_id: product1.id)
      get :index
      expect(assigns(:cart_items)).to eq([cart_item1, cart_item2])
    end

    it "renders the index view page for cart items" do
      get :index
      expect(response).to render_template(:index)
    end

  end

  describe 'post create' do

    before(:each) do
      sign_in user1
    end

    context "Same product exists earlier in the cart" do

      it "increments the quantity of cart item" do
        cart_item1 = FactoryGirl.create(:cart_item, quantity: 10, cart_id: user1.cart.id, product_id: product1.id)
        post :create, params: { cart_item: FactoryGirl.attributes_for(:cart_item, product_id: product1.id) }
        expect(assigns(:cart_item).quantity).to eq(20)
      end

      it "redirects to the marketplace index page if add to cart is clicked" do
        cart_item1 = FactoryGirl.create(:cart_item, quantity: 10, cart_id: user1.cart.id, product_id: product1.id)
        post :create, params: { cart_item: FactoryGirl.attributes_for(:cart_item, product_id: product1.id), commit: "Add to cart" }
        expect(response).to redirect_to('/marketplace')
      end

      it "redirects to the marketplace index page if add to cart is clicked" do
        cart_item1 = FactoryGirl.create(:cart_item, quantity: 10, cart_id: user1.cart.id, product_id: product1.id)
        post :create, params: { cart_item: FactoryGirl.attributes_for(:cart_item, product_id: product1.id), commit: "Buy" }
        expect(response).to redirect_to('/cart_items')
      end

    end

    context "Same product does not exist earlier in the cart" do

      it "makes a new @cartitem and saves it to database" do
        expect{ post :create, params: { cart_item: FactoryGirl.attributes_for(:cart_item, product_id: product1.id) }}.to change(CartItem, :count).by(1)
      end

      it "redirects to the marketplace index page if add to cart is clicked" do
        post :create, params: { cart_item: FactoryGirl.attributes_for(:cart_item, product_id: product1.id), commit: "Add to cart" }
        expect(response).to redirect_to('/marketplace')
      end

      it "redirects to the marketplace index page if add to cart is clicked" do
        post :create, params: { cart_item: FactoryGirl.attributes_for(:cart_item, product_id: product1.id), commit: "Buy" }
        expect(response).to redirect_to('/cart_items')
      end

    end

  end

  describe 'delete destroy' do
    
    before(:each) do
      sign_in user1
      @cart_item1 = FactoryGirl.create(:cart_item, cart_id: user1.cart.id, product_id: product1.id)
    end

    it "deletes the requested object" do
      expect{ delete :destroy, params: {id: @cart_item1} }.to change(CartItem, :count).by(-1)
    end

    it "redirects to the index page of products" do
      delete :destroy, params: {id: @cart_item1}
      expect(response).to redirect_to('/cart_items')
    end

  end

end