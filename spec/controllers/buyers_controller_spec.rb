require 'rails_helper'

RSpec.describe BuyersController, type: :controller do

  let (:user1) {FactoryGirl.create(:user)} 
  let (:user2)  {FactoryGirl.create(:user)}
  let (:user3) {FactoryGirl.create(:user)}
  let (:invoice1)  {FactoryGirl.create(:invoice, user_id: user1.id)}
  let (:invoice2)  {FactoryGirl.create(:invoice, user_id: user2.id)}

  describe 'get index' do
    it "selects buyers from all users" do
      user3.add_role :admin
      user3.remove_role :buyer
      sign_in user3
      user1.add_role :buyer
      user2.add_role :buyer  
      get :index
      expect(assigns(:buyers)).to include(user1,user2)
    end

    it "renders the index view " do
      user3.add_role :admin
      sign_in user3
      get :index
      expect(response).to render_template(:index)
    end

  end

  describe 'get invoices' do
    it "renders the list of invoices " do
      user3.add_role :admin
      sign_in user3
      get :invoices, params: {id: user1.id}
      expect(response).to render_template('buyers/invoices')
    end

    it "selects all invoices of a particular buyer" do
      user3.add_role :admin
      sign_in user3
      get :invoices, params: {id: user1.id}
      expect(assigns(:invoices)). to eq([invoice1])
    end

  end

end

