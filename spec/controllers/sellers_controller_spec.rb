require 'rails_helper'

RSpec.describe SellersController, type: :controller do


  let (:user1) {FactoryGirl.create(:user)} 
  let (:user2)  {FactoryGirl.create(:user)}
  let (:user3)  {FactoryGirl.create(:user)}

  describe 'get index' do
    it "selects sellers from all users" do
      user3.add_role :admin
      user3.remove_role :buyer
      sign_in user3
      user1.add_role :buyer
      user2.add_role :seller  
      get :index
      expect(assigns(:sellers)).to eq([user2])
    end

    it "renders the index view " do
      user3.add_role :admin
      user3.remove_role :buyer
      sign_in user3
      get :index
      expect(response).to render_template(:index)
    end

  end

end
