require 'rails_helper'

RSpec.describe PermissionsController, type: :controller do

  let (:user1) {FactoryGirl.create(:user)} 
  let (:user2)  {FactoryGirl.create(:user)}
  let (:user3) {FactoryGirl.create(:user)}

  describe 'get index' do
    
    before(:each) do
      user3.add_role :admin
      user3.remove_role :buyer
      sign_in user3
      user1.add_role :buyer
      user2.add_role :seller
    end

    it "assigns all users to @users" do
      get :index
      expect(assigns(:users)).to include(user1,user2,user3)
    end

    it "renders the index page for permissions" do
      get :index
      expect(response).to render_template(:index)
    end

  end

  describe 'get buyers' do
    
    before(:each) do
      user3.add_role :admin
      user3.remove_role :buyer
      sign_in user3
      user1.add_role :buyer
      user2.add_role :seller
    end

    it "assigns all buyers to @buyers" do
      get :buyers
      expect(assigns(:buyers)).to include(user1, user2)
      expect(assigns(:buyers)).not_to include(user3)

    end

    it "renders the index page for permissions" do
      get :buyers
      expect(response).to render_template('permissions/buyers')
    end

  end

  describe 'get sellers' do
    
    before(:each) do
      user3.add_role :admin
      user3.remove_role :buyer
      sign_in user3
      user1.add_role :buyer
      user2.add_role :seller
    end

    it "assigns all sellers to @sellers" do
      get :sellers
      expect(assigns(:sellers)).to include(user2)
      expect(assigns(:sellers)).not_to include(user1, user3)
    end

    it "renders the index page for permissions" do
      get :sellers
      expect(response).to render_template('permissions/sellers')
    end

  end

  describe 'put update role' do

    before(:each) do
      user3.add_role :admin
      user3.remove_role :buyer
      sign_in user3
      user1.add_role :buyer
      user2.add_role :seller
    end

    it "locates the user and stores it in @user" do
      put :update_role, params: {user_id: user1.id, checked: true}
      expect(assigns(:user)).to eq(user1)
    end

    it "changes adds or removes the seller role depending on value of checked" do
      put :update_role, params: {user_id: user1.id, checked: true}
      expect(assigns(:user).is_seller?).to eq(true)
    end
    
  end

end

