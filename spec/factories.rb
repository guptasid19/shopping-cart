require 'ffaker'

FactoryGirl.define do
  
  factory :user do
    first_name "sid"
    last_name "gupta"
    email { FFaker::Internet.email }
    password "secret"
  end

  factory :invoice do
    status "placed"
    shipping_address "indore"
  end

  factory :product do
    name "random"
    brand "random"
    price "70"
  end

  factory :cart_item do
    quantity 10
  end

  factory :invoice_detail do
    quantity 10
  end
  
end