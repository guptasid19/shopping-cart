require 'twilio-ruby'
class InvoiceOrderSmsWorker

  include Sidekiq::Worker

  def perform(user_id, invoice_id)
    user = User.where(id: user_id).first
    invoice = Invoice.where(id: invoice_id).first
    message = "Your order with a total of #{invoice.total} placed. Thank you for shopping with ShopCon."
    user.send_sms(message)
  end
  
end