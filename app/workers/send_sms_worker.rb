class SendSmsWorker
  include Sidekiq::Worker

  def perform(user_id)
    user = User.where(id: user_id).first
    message = "User with email #{user.email} updated name to #{user.first_name}"
    user.send_sms(message)
  end
  
end