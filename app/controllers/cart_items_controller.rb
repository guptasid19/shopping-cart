class CartItemsController < BaseController

  def index
    @cart_items = current_user.cart.cart_items
  end

  def create
    @cart_item = current_user.cart.cart_items.where(product_id: params[:cart_item][:product_id]).first_or_initialize  
    if @cart_item.quantity.nil? == false      
      @cart_item.increment!(:quantity, params[:cart_item][:quantity].to_i)
      flash[:notice] = "Successfully added #{@cart_item.product.name} item to cart."
      redirect_to (params[:commit] == 'Buy') ? cart_items_path : marketplace_index_path
    else
      @cart_item.quantity = params[:cart_item][:quantity].to_i
      if @cart_item.save
        flash[:notice] = "Successfully added #{@cart_item.product.name} item to cart."
        redirect_to (params[:commit] == 'Buy') ? cart_items_path : marketplace_index_path
      else
        flash[:notice] = "Item could not be added to cart."
        redirect_to marketplace_index_path
      end
    end 
  end

  def destroy
    @cart_item = current_user.cart.cart_items.where(id: params[:id]).first
    if @cart_item.destroy
      flash[:notice] = "Item deleted from cart."
    else
      flash[:notice] = "Item could not be deleted from cart."
    end
    redirect_to cart_items_path
  end

  private

  def cart_item_params
    params.require(:cart_item).permit(:product_id,:quantity)
  end

end
