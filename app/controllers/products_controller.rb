class ProductsController < BaseController
  
  before_action :require_admin_or_seller

  def index
    @products = current_user.products
  end

  def new
    @product = current_user.products.new
    @product.build_picture
  end

  def create
    product = current_user.products.new(product_params)    
    if product.save
      flash[:notice] = "Product successfully added."
      redirect_to products_path
    else
      flash[:notice] = "Product could not be created."
      redirect_to products_path
    end
  end

  def show
    @product = current_user.products.where(id: params[:id]).first
    if @product.blank?
      flash[:notice] = "Product not found."
      redirect_to products_path
    end  
  end  

  def destroy
    product = current_user.products.where(id: params[:id]).first
    if product.destroy
      flash[:notice] = "Product deleted successfully."
      redirect_to products_path
    else
      flash[:notice] = "Product could not be deleted."
      redirect_to products_path
    end
  end


  def edit
    @product = current_user.products.where(id: params[:id]).first
  end

  def update
    @product = current_user.products.where(id: params[:id]).first
    if @product.update(product_params)
      flash[:notice] = "Product successfully updated."
      redirect_to products_path
    else
      flash[:error] = @product.errors.full_messages.join(',')
      render 'edit'
    end
  end

  private

  def product_params
    params.require(:product).permit(:name, :brand, :category, :price, :stock, :description, picture_attributes: [:image])
  end

  def require_admin_or_seller
    unless current_user && ((current_user.has_role? :admin) || (current_user.has_role? :seller))
      flash[:notice] = "You dont't have the privileges to view this page"
      redirect_to root_path
    end     
  end

end