class ApplicationController < ActionController::Base

  protect_from_forgery with: :exception

  before_action :configure_permitted_parameters, if: :devise_controller?

  def after_sign_in_path_for(resource)
    ((current_user.has_role? :seller) || (current_user.has_role? :admin)) ? dashboard_index_path : marketplace_index_path
  end

  def configure_permitted_parameters
    added_attrs = [:address, :first_name, :last_name, :phone, :dob, picture_attributes: [:image]]
    devise_parameter_sanitizer.permit(:sign_up, keys: added_attrs)
    devise_parameter_sanitizer.permit(:account_update, keys: added_attrs)
  end

end