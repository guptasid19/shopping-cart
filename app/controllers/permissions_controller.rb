class PermissionsController < BaseController

  before_action :require_admin

  def index
    @users = User.all
  end

  def buyers
    @buyers = User.all.select{ |user| user.is_buyer? }
  end

  def sellers
    @sellers = User.all.select{ |user| user.is_seller? }
  end

  def update_role
    @user = User.where(id: params[:user_id]).first
    if @user && @user.change_role(params[:checked])
      notice = "Role changed."
    elsif user.is_nil?
      notice = "User not found and role not changed."
    else
      notice = "Role not changed."
    end
    respond_to do |format|
      format.html {redirect_to permissions_path, notice: notice}
    end
  end  
  
  private

  def require_admin
    unless current_user && (current_user.has_role? :admin)
      flash[:notice] = "You are not an admin"
      redirect_to root_path
    end        
  end
  
end