class ChargesController < ApplicationController

  before_action :authenticate_user!

  def create

    @amount = params[:total].to_i
    @invoice = current_user.invoices.where(id: params[:invoice_id]).first

    customer = Stripe::Customer.create(
      :email => params[:stripeEmail],
      :source  => params[:stripeToken]
    )

    charge = Stripe::Charge.create(
      :customer    => customer.id,
      :amount      => @amount,
      :description => 'Rails Stripe customer',
      :currency    => 'usd'
    )
    
    flash[:notice] = "Your payment of Rs. #{@amount} was successful."
    @invoice.status = 'Order Placed and Paid'
    @invoice.payment_method = 'Card Payment'
    @invoice.save
    redirect_to invoices_path
    
    rescue Stripe::CardError => e
      flash[:error] = e.message
      redirect_to invoices_path   

  end

end
