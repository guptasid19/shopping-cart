class InvoicesController < BaseController
  
  def index
    @invoices = current_user.invoices
  end

  def new
    @cart_items = current_user.cart.cart_items
  end

  def create
    @invoice = current_user.invoices.new(invoice_params)
    @invoice.initialize_invoice_from_cart(current_user)
    if @invoice.save
      current_user.cart.cart_items.destroy_all
      flash[:notice] = "Order successfully added. Please proceed with payment to place your order."
      redirect_to invoice_path(@invoice)
    else
      flash[:notice] = "Order could not be placed."
      redirect_to marketplace_index_path
    end    
  end


  def show
    @invoice = current_user.invoices.where(id: params[:id]).first
    if @invoice.nil?
      flash[:notice] = "Invoice could not be found."
      redirect_to invoices_path
    end
  end

  private
  
  def invoice_params
    params.require(:invoice).permit(:shipping_address, :total)
  end

end