class BuyersController < ApplicationController

  before_action :require_admin
  def index
    @buyers = User.select{ |user| user.is_buyer? }
  end

  def invoices
    @invoices = Invoice.where(user_id: params[:id]).all
  end
  
  private

  def require_admin
    unless current_user && (current_user.has_role? :admin)
      flash[:notice] = "You are not an admin"
      redirect_to root_path
    end        
  end

end
