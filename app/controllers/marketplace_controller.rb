class MarketplaceController < ApplicationController

  def index
    @products = Product.search(params[:search]).paginate(page: params[:page], per_page: 8)
  end

  def show
    @product = Product.where(id: params[:id]).first
    if @product.nil?
      flash[:notice] = "Product could not be found."
      redirect_to marketplace_index_path
    end
  end

end
