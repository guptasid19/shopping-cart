class DashboardController < BaseController
  
  before_action :require_admin_or_seller

  def index
    if current_user.is_seller?
      @cartitems = CartItem.get_seller_items(current_user.id)
      @invoice_items = InvoiceDetail.get_seller_items(current_user.id)
    end  
  end
  
  private
 

  def require_admin_or_seller
    unless current_user && ((current_user.has_role? :admin) || (current_user.has_role? :seller))
      flash[:notice] = "You dont't have the privileges to view this page"
      redirect_to root_path
    end     
  end

end
