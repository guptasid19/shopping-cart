class SellersController < ApplicationController

  before_action :require_admin

  def index
    @sellers = User.select{ |user| user.is_seller? }
  end

  private

  def require_admin
    unless current_user && (current_user.has_role? :admin)
      flash[:notice] = "You are not an admin"
      redirect_to root_path
    end        
  end
  
end
