class Product < ApplicationRecord
  
  belongs_to :user
  has_many :cart_items
  has_many :carts, through: :cart_items
  has_many :invoice_details
  has_many :invoices, through: :invoice_details
  has_one :picture, as: :imageable, dependent: :destroy

  accepts_nested_attributes_for :picture

  def self.search(search)
    if search
      where('name LIKE ?', "%#{search}%")
    else
      unscoped
    end
  end

end
