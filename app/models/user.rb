require 'twilio.rb'
class User < ApplicationRecord
  include Twilio

  has_many :products
  has_one :picture, as: :imageable, dependent: :destroy
  has_one :cart, dependent: :destroy
  has_many :invoices, dependent: :destroy
  rolify
  
  scope :has_cart_item, lambda { |id| joins(:cart).where( carts: {id: id}).first}

  after_create :assign_default_role, :create_cart
  after_update :send_sms_to_admin

  accepts_nested_attributes_for :picture
  #Validations
  validates :first_name, presence: true, length: {maximum:20}
  validates :last_name, presence: true, length: {maximum:20}
  validates :phone, uniqueness: true, allow_blank: true
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable,
         :omniauthable, omniauth_providers: [:facebook, :google_oauth2]

  def assign_default_role
    self.add_role(:buyer) if self.roles.blank?
  end
    
  def is_admin?
    has_role? :admin
  end

  def is_seller?
    has_role? :seller
  end

  def is_buyer?
    has_role? :buyer
  end

  def change_role(operation) 
    operation == "true" ? (self.add_role :seller) : (self.remove_role :seller) 
  end

  def self.new_with_session(params, session)
    super.tap do |user|
      if data = session["devise.facebook_data"] && session["devise.facebook_data"]["extra"]["raw_info"]
        user.email = data["email"] if user.email.blank?
      end
    end
  end

  def self.from_omniauth(auth)
    where(provider: auth.provider, uid: auth.uid).first_or_create do |user|
      user.email = auth.info.email
      user.password = Devise.friendly_token[0,20]
      user.first_name = auth.info.name.split.first
      user.last_name = auth.info.name.split.last
      user.profile_pic = auth.info.image
    end
  end
  private

  def send_sms_to_admin
    delete_existing_worker
    SendSmsWorker.perform_in(10.seconds, self.id) 
  end


  def create_cart
    if self.cart.blank?
      cart = self.build_cart     
      cart.save
    end
  end

  def delete_existing_worker
    jobs = Sidekiq::ScheduledSet.new
    jobs.each do |job|
      job.delete if job.klass == 'SendSMSWorker' && job.args.include?(id)
    end
  end

end
