class Picture < ApplicationRecord
  mount_uploader :image, AvatarUploader
  belongs_to :imageable, polymorphic: true, optional: true

end
