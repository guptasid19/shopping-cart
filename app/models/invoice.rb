class Invoice < ApplicationRecord
  attr_accessor :quantity, :product_id
  belongs_to :user
  has_many :invoice_details, dependent: :destroy
  has_many :products, through: :invoice_details

  after_save :send_sms

  def initialize_invoice_from_cart(user)
    self.status = 'Order Placed'
    self.payment_method = 'Cash On Delivery'
    cart_items = user.cart.cart_items.all
    cart_items.each do |item|
      self.invoice_details.new(product_id: item.product_id, quantity: item.quantity.to_i, price: item.product.price)
      update_product(item)
    end
  end

  private 

  def send_sms
    InvoiceOrderSmsWorker.perform_in(30.seconds, self.user_id, self.id)
  end

  def update_status
    self.status = "Order Placed"   
  end

  def update_product(item)
    (item.product.stock - item.quantity > 0) ? item.product.decrement!(:stock, item.quantity) : item.product.update_attribute(:stock, 0)
  end

end