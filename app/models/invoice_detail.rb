class InvoiceDetail < ApplicationRecord
  belongs_to :invoice
  belongs_to :product

  after_create_commit { UpdateSoldProductsJob.perform_later self}

  scope :get_seller_items, lambda { |user_id| joins(:product).where(products: {user_id: user_id} ) }
  
  def subtotal
    self.quantity * self.price
  end

end
