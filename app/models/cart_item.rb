class CartItem < ApplicationRecord
  belongs_to :cart
  belongs_to :product

  after_create_commit { UpdateAddedToCartProductsJob.perform_later self}

  scope :get_seller_items, lambda { |user_id| joins(:product).where( products: {user_id: user_id}) }
end
