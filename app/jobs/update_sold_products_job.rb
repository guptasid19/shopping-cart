class UpdateSoldProductsJob < ApplicationJob
  queue_as :default

  def perform(invoice_item)
    # Do something later
    user =  User.joins(:invoices).where(invoices: {id: invoice_item.invoice_id}).first
    ActionCable.server.broadcast "update_channel_#{invoice_item.product.user_id}", message: render_invoice_item(invoice_item), name: user.first_name, product_name: invoice_item.product.name, cart: false
  end

  private

  def render_invoice_item(invoice_item)
    DashboardController.render(partial: 'invoice_item', locals: {item: invoice_item})
  end

end
