class UpdateAddedToCartProductsJob < ApplicationJob
  queue_as :default

  def perform(cart_item)
    # Do something later
    user =  User.joins(:cart).where(carts: {id: cart_item.cart_id}).first
    ActionCable.server.broadcast "update_channel_#{cart_item.product.user_id}",
                                message: render_cart_item(cart_item),
                                name: user.first_name,
                                product_name: cart_item.product.name,
                                cart: true
  end

  private

  def render_cart_item(cart_item)
    DashboardController.render(partial: 'cart_item', locals: {item: cart_item})
  end

end