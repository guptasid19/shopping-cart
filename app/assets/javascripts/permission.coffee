# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

$(document).on 'change', '#Allow_to_Sell', ->
  $.ajax
    type: 'PUT'
    url: '/permissions/update_role'
    data:
      user_id: $(this).data("user-id")
      checked: $(this).is(':checked')
  return
