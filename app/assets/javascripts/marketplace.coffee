# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
$(document).on 'click', '.products .paginate a', ->
  $.getScript @href
  false

$(document).on 'keyup', '.products_search', ->
  $.get @action, $(this).serialize(), null, 'script'
  false

$(document).on 'submit', '.products_search', ->
  $.get @action, $(this).serialize(), null, 'script'
  true