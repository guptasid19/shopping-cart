// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, or any plugin's
// vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require bootstrap-sprockets
//= require rails-ujs
//= require turbolinks
//= require_tree .

$(document).ready(function () {

  // ANIMATEDLY DISPLAY THE NOTIFICATION COUNTER.
  $('#noti_Counter')
    .css({ opacity: 0 })
    .text('0')              // ADD DYNAMIC VALUE (YOU CAN EXTRACT DATA FROM DATABASE OR XML).
    .css({ top: '-10px' })
    .animate({ top: '-2px', opacity: 1 }, 500)
    .hide();

  $('#noti_Button').click(function () {

    // TOGGLE (SHOW OR HIDE) NOTIFICATION WINDOW.
    $('#notifications').fadeToggle('fast', 'linear', function () {
        if ($('#notifications').is(':hidden')) {
            $('#noti_Button').css('background-color', '#2E467C');
        }
        else $('#noti_Button').css('background-color', '#FFF');        // CHANGE BACKGROUND COLOR OF THE BUTTON.
    });

    $('#noti_Counter').fadeOut('slow');                 // HIDE THE COUNTER.

    return false;
  });

  // HIDE NOTIFICATIONS WHEN CLICKED ANYWHERE ON THE PAGE.
  $(document).click(function () {
    $('#notifications').hide();
    // CHECK IF NOTIFICATION COUNTER IS HIDDEN.
    if ($('#noti_Counter').is(':hidden')) {
        // CHANGE BACKGROUND COLOR OF THE BUTTON.
        $('#noti_Button').css('background-color', '#2E467C');
    }
  });

  $('#notifications').click(function () {
    return false;       // DO NOTHING WHEN CONTAINER IS CLICKED.
  });
});