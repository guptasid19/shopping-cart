App.update = App.cable.subscriptions.create "UpdateChannel",
  connected: ->
    # Called when the subscription is ready for use on the server

  disconnected: ->
    # Called when the subscription has been terminated by the server

  received: (data) ->
    # Called when there's incoming data on the websocket for this channel
    $('#noti_Counter').text (parseInt($('#noti_Counter').text()) + 1).toString()
    $('#noti_Counter').show()
    if data.cart == false
      $('#dashboard-list-bottom').append data.message
      $('.h3-class').after '<div class="notifications-data">' + ' ' + data.name + ' ' + 'bought ' + data.product_name + ' </div>'
    else
      $('#dashboard-list-top').append data.message
      $('.h3-class').after '<div class="notifications-data">' + ' ' + data.name + ' ' + 'added ' + data.product_name + ' to his cart' + ' </div>'

