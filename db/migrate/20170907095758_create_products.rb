class CreateProducts < ActiveRecord::Migration[5.1]
  def change
    create_table :products do |t|
      t.string :name
      t.string :brand
      t.string :category
      t.float :price
      t.integer :stock
      t.text :description

      t.timestamps
    end
  end
end
