class AddPaymentMethodToinvoice < ActiveRecord::Migration[5.1]
  def change
    add_column :invoices, :payment_method, :string
  end
end
