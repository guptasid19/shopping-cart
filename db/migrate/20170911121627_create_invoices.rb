class CreateInvoices < ActiveRecord::Migration[5.1]
  def change
    create_table :invoices do |t|
      t.string :status
      t.date :order_date
      t.date :delivery_date
      t.text :shipping_address
      t.float :total

      t.timestamps
    end
  end
end
