class DeleteOrderDateFromInvoice < ActiveRecord::Migration[5.1]
  def change
    remove_column :invoices, :order_date
  end
end
