class AddKeystoInvoiceDetails < ActiveRecord::Migration[5.1]
  def change
    add_reference :invoice_details, :invoice, foreign_key: true
    add_reference :invoice_details, :product, foreign_key: true
  end
end
