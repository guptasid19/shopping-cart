class RemovePictureFromProducts < ActiveRecord::Migration[5.1]
  def change
    remove_column :products, :picture
  end
end
