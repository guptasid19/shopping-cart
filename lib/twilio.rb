require 'twilio-ruby'
module Twilio
  
  def send_sms(message)
    client = Twilio::REST::Client.new ENV['twilio_account_sid'], ENV['twilio_auth_token']
    client.api.account.messages.create(
      from: ENV['twilio_sender_number'],
      to: self.phone,
      body: message
    )
  end

end